<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel Link Shortner

Laravel Link Shortner is simple application, aim for data collection and shortlink generator. This application is build based on Laravel framework version 9.0. [Laravel](https://laravel.com) is a web application framework with expressive, elegant syntax.

This system develop using:

- **[Laravel 9.0](https://laravel.com/)**
- **[PHP 8.1](https://php.net/)**
- **[Bootstrap 5](https://getbootstrap.com/)**

### Develop By

- **[Fikri Mastor](https://www.fikrimastor.com/)**
- **[LinkedIn](https://www.linkedin.com/in/fikrimastor/)**

### Clone Repository

Clone this repository into local machine.

```bash
git clone git@gitlab.com:fikrimastor/shortly.git
```

### Copy ENV File

Run copy .env file to setup your env.

```bash
cp .env.example .env
```

### Install Composer

Run composer install to install required dependency.

```bash
composer install
```

### Run Migration

After setup the database credentials in .env file, you may run migration.

```bash
php artisan migrate
```

### Generate key

Generate key for your app and test run.

```bash
php artisan key:generate
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
